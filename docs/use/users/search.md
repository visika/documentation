# How to search with Mobilizon

!!! info
    Search match:

      * for events: with titles, tags and descriptions
      * for groups: only with names (for now)

A quick search is available in **Search** field, in top bar menu:

![img top bar search](../../images/en/search-bar-top-EN.png)

You can go directly to the advanced search by clicking on **Explore** in the top navigation bar of the site. Options are:

  * **Key words**: tags or words in title or/and description
  * **Location**: to filter events in a specific location
  * **Radius**: to widen the results of the place at a certain distance
  * **Date**: to filter events according to when they take place (not available for group search)

![search explore page](../../images/en/search-explore-EN.png)

## Example

For instance, if you want to find a collage event you will enter `collage`:

![img collage search no filter](../../images/en/search-collage-no-filter-EN.png)

If you want to limit to **London** and surroundings:

![img collage location filter](../../images/en/search-collage-location-filter-EN.png)

Or, to find a **group**, by clicking **Groups** tab:

![image group search](../../images/en/search-group-contrib-EN.png)
