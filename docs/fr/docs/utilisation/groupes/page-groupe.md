# Page d'un groupe

## Rejoindre un groupe

Si le groupe est [configuré pour permettre à n'importe qui de rejoindre le groupe sans invitation](../../groupes/creation-groupe/#parametres), vous devez, sur la page du groupe, cliquer sur le bouton **Rejoindre le groupe**. Si vous n'êtes pas connecté⋅e à votre compte, vous serez invité⋅e à le faire.

## Suivre les actualités publiques d'un groupe

Il est possible de suivre les actualités publiques d'un groupe en utilisant&nbsp;:

   * les flux RSS∕Atom à utiliser avec des lecteurs (web/applications) 
   * ICS/WebCal à utiliser avec les calendriers supportant ces formats. 
   
Pour ce faire, vous devez, sur la page du groupe&nbsp;:
  
  1. cliquer sur **⋅⋅⋅**
  * faire un *clic-droit* sur **Flux Atom pour les événements et les billets** ou sur **Flux ICS pour les événements** 
  * cliquer sur **Copier l'adresse du lien** pour copier l'adresse à coller.
  
![image montrant le menu déroulant avec les options](../../images/rss-group-FR.png)

## Signaler un groupe

Pour signaler un groupe, vous devez&nbsp;:

  1. cliquer sur le bouton **⋅⋅⋅**
  * cliquer sur le bouton **Signalement**

    ![bouton de signalement](../../images/report-group-FR.png)

  * [Optionnel mais **recommandé**] renseigner la raison du signalement&nbsp;:
    ![modale de signalement](../../images/report-group-modal-FR.png)
